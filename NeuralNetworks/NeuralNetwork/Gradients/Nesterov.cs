﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.GradientAdjustmentParameters;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.Gradients
{
    class Nesterov:IGradient
    {
        private double LearningRate { get; }

        private double Momentum { get; }

        private Matrix<double> Velocity { get; set; }

        public Nesterov(int batchsize, int layersize, double learningRate, double momentum)
        {
            LearningRate = learningRate;

            Momentum = momentum;

            Velocity = Matrix<double>.Build.Dense(layersize, batchsize, 0.0);

        }

        public Matrix<double> ComputeGrad(Matrix<double> grad, Matrix<double> theta)
        {
            theta = theta + Momentum * Momentum * Velocity - LearningRate * (1 + Momentum) * grad;
            Velocity = Momentum * Velocity - LearningRate * grad;
            return theta;
        }

       
    }
}
