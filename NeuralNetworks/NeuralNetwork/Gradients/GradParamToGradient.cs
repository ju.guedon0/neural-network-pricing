﻿using NeuralNetwork.Common.GradientAdjustmentParameters;
using NeuralNetwork.Common.Layers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.Gradients
{
    class GradParamToGradient
    {
        
        
        public IGradient convertParamToGradient(IGradientAdjustmentParameters gradientAdjustmentParameters, int batchSize, int layerSize)
        {
            switch (gradientAdjustmentParameters.Type)
            {
                case GradientAdjustmentType.FixedLearningRate:
                    {
                        double rate = ((FixedLearningRateParameters)gradientAdjustmentParameters).LearningRate;
                        return new FixedLearningRate(rate); 
                    }
                case GradientAdjustmentType.Momentum:
                    {
                        double rate = ((MomentumParameters)gradientAdjustmentParameters).LearningRate;
                        double momentum = ((MomentumParameters)gradientAdjustmentParameters).Momentum;
                        return new MomentumGradient(batchSize,layerSize, rate, momentum);
                    }
                case GradientAdjustmentType.Adam:
                    {
                        double stepsize = ((AdamParameters)gradientAdjustmentParameters).StepSize;
                        double firstMomentDecay = ((AdamParameters)gradientAdjustmentParameters).FirstMomentDecay;
                        double SecondMomentDecay = ((AdamParameters)gradientAdjustmentParameters).SecondMomentDecay;
                        double denominatorFactor = ((AdamParameters)gradientAdjustmentParameters).DenominatorFactor;
                        return new AdamGradient(stepsize, firstMomentDecay, SecondMomentDecay, denominatorFactor, batchSize, layerSize);
                    }
                case GradientAdjustmentType.Nesterov:
                {
                double rate = ((NesterovParameters)gradientAdjustmentParameters).LearningRate;
                    double momentum = ((NesterovParameters)gradientAdjustmentParameters).Momentum;
                    return new Nesterov(batchSize, layerSize, rate, momentum);
            }
                default:
                    {
                        throw new InvalidOperationException("Unknown GradientAdjustmentType: ");
                    }
            }
        }
    }
}
