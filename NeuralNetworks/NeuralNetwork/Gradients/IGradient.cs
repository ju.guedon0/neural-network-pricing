﻿using System;
using System.Collections.Generic;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.GradientAdjustmentParameters;

namespace NeuralNetwork.Gradients
{
    public interface IGradient
    {
        Matrix<double> ComputeGrad(Matrix<double> grad, Matrix<double> theta);

    }
}
