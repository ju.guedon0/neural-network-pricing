﻿using System;
using NeuralNetwork.Common.GradientAdjustmentParameters;
using MathNet.Numerics.LinearAlgebra;

namespace NeuralNetwork.Gradients
{
    public class MomentumGradient : IGradient
    {

        private double LearningRate { get; }

        private double Momentum { get; }

        private Matrix<double> Velocity { get; set; }


        public MomentumGradient(int batchsize, int layersize, double learningRate, double momentum)
        {
            LearningRate = learningRate; 

            Momentum = momentum; 

            Velocity = Matrix<double>.Build.Dense(layersize, batchsize, 0.0);

        }

        public Matrix<double> ComputeGrad(Matrix<double> grad, Matrix<double> theta)
        {
            Velocity = Momentum * Velocity - LearningRate * grad;
            return theta + Velocity;
        }

    }
}
