﻿using System;
using System.Collections.Generic;
using System.Text;
using NeuralNetwork.Common.GradientAdjustmentParameters;
using MathNet.Numerics.LinearAlgebra;

namespace NeuralNetwork.Gradients
{
    class FixedLearningRate : IGradient
    {

        public IGradientAdjustmentParameters GradParameters { get; set; }

        private double LearningRate { get; }


        public FixedLearningRate(double learningRate)
        {
            LearningRate = learningRate; 

        }

        public Matrix<double> ComputeGrad(Matrix<double> grad, Matrix<double> theta)
        {
            return theta - LearningRate * grad;
        }


    }
}
