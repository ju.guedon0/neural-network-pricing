﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.GradientAdjustmentParameters;
using System;

namespace NeuralNetwork.Gradients
{
    class AdamGradient : IGradient
    {
        private double StepSize { get; }

        private double FirstMomentDecay { get; }

        private double SecondMomentDecay { get; }

        private double DenominatorFactor { get; }

        private Matrix<double> V { get; set; }

        private Matrix<double> Sp { get; set; }

        private Matrix<double> S { get; set; }

        private Matrix<double> Rp { get; set; }

        private Matrix<double> R { get; set; }

        public int NbTrainingSteps { get; set; }


        public AdamGradient(double stepSize, double firstMomentDecay, double secondMomentDecay, double denominatorFactor,
            int layersize, int batchsize)
        {
            StepSize = stepSize;

            FirstMomentDecay = firstMomentDecay;

            SecondMomentDecay = secondMomentDecay;

            DenominatorFactor = denominatorFactor;

            V = Matrix<double>.Build.Dense(batchsize, layersize, 0.0);

            Sp = Matrix<double>.Build.Dense(batchsize, layersize, 0.0);

            S = Matrix<double>.Build.Dense(batchsize, layersize, 0.0);

            Rp = Matrix<double>.Build.Dense(batchsize, layersize, 0.0);

            R = Matrix<double>.Build.Dense(batchsize, layersize, 0.0);

            NbTrainingSteps = 1;
        }

        public Matrix<double> ComputeGrad(Matrix<double> grad, Matrix<double> theta)
        {
            S = FirstMomentDecay * S + (1 - FirstMomentDecay) * grad;
            R = SecondMomentDecay * R + (1 - SecondMomentDecay) * (grad.PointwiseMultiply(grad));
            Sp = S / (1 - Math.Pow(FirstMomentDecay, NbTrainingSteps));
            Rp = R / (1 - Math.Pow(SecondMomentDecay, NbTrainingSteps));
            V = (-StepSize * Sp).PointwiseDivide(DenominatorFactor + Rp.PointwiseSqrt());
            return theta + V;

        }
    }
}
