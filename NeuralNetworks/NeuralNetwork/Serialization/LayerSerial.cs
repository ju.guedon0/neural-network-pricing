﻿using NeuralNetwork.Common.GradientAdjustmentParameters;
using NeuralNetwork.Common.Layers;
using NeuralNetwork.Common.Serialization;
using NeuralNetwork.Layers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.Serialization
{
    class LayerSerial
    {
        

        public ISerializedLayer Serialize(ILayer layer)
        {
            switch (layer)
            {
                case BasicStandardLayer standardLayer:
                    return SerializeStandardLayer(standardLayer);
                case L2PenaltyLayer standardLayer:
                    return SerializeL2PenaltyLayer(standardLayer);
                case WeightDecayLayer standardLayer:
                    return SerializeWeightDecayLayer(standardLayer);
                case InputStandardizingLayer standardLayer:
                    return SerializeInputStandardizingLayer(standardLayer);
                default:
                    throw new InvalidOperationException("Unknown layer type: " + layer.GetType());
            }
        }
        private ISerializedLayer SerializeStandardLayer(BasicStandardLayer standardLayer)
        {
            var bias = standardLayer.Bias.ToColumnArrays()[0];
            var weights = standardLayer.Weights.ToArray();
            var activatorType = standardLayer.Activator.Type;
            var adjustmentParameters = standardLayer.GradientAdjustmentParameters;
            return new SerializedStandardLayer(bias, weights, activatorType, adjustmentParameters);
        }
        private ISerializedLayer SerializeL2PenaltyLayer(L2PenaltyLayer l2PenaltyLayer)
        {
            ISerializedLayer serializedStandardLayer = Serialize(l2PenaltyLayer.UnderlyingLayer);
            return new SerializedL2PenaltyLayer(serializedStandardLayer, l2PenaltyLayer.PenaltyCoefficient);
        }
        private ISerializedLayer SerializeWeightDecayLayer(WeightDecayLayer weightDecayLayer)
        {
            ISerializedLayer serializedStandardLayer = Serialize(weightDecayLayer.UnderlyingLayer);
            return new SerializedWeightDecayLayer(serializedStandardLayer, weightDecayLayer.DecayRate);
        }
        private ISerializedLayer SerializeInputStandardizingLayer(InputStandardizingLayer inputStandardizingLayer)
        {
            ISerializedLayer serializedLayer = Serialize(inputStandardizingLayer.UnderlyingLayer);
            return new SerializedInputStandardizingLayer(serializedLayer, inputStandardizingLayer.Mean, inputStandardizingLayer.StdDev); ;
        }
        private ISerializedLayer SerializeDropoutLayer(Dropout dropoutLayer)
        {
            return new SerializedDropoutLayer(dropoutLayer.LayerSize, dropoutLayer.Proba);
        }
    }
}
        
