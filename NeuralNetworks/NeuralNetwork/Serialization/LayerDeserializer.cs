﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Activators;
using NeuralNetwork.Common.Layers;
using NeuralNetwork.Common.Serialization;
using NeuralNetwork.Layers;
using System;

namespace NeuralNetwork.Serialization
{
    internal static class LayerDeserializer
    {
        public static ILayer Deserialize(ISerializedLayer serializedLayer, int batchSize)
        {
            switch (serializedLayer.Type)
            {
                case LayerType.Standard:
                    var standardSerialized = serializedLayer as SerializedStandardLayer;
                    return DeserializeStandardLayer(standardSerialized, batchSize);

                case LayerType.L2Penalty:
                    var l2PenaltySerialized = serializedLayer as SerializedL2PenaltyLayer;
                    return DeserializeL2PenaltyLayer(l2PenaltySerialized, batchSize);

                case LayerType.WeightDecay:
                    var WeightDecaySerialized = serializedLayer as SerializedWeightDecayLayer;
                    return DeserializeWeightDecayLayer(WeightDecaySerialized, batchSize);

                case LayerType.InputStandardizing:
                    var InputStandardizingSerialized = serializedLayer as SerializedInputStandardizingLayer;
                    return DeserializeInputStandardizingLayer(InputStandardizingSerialized, batchSize);

                case LayerType.Dropout:
                    var dropoutSerialized = serializedLayer as SerializedDropoutLayer;
                    return DeserializeDropoutLayer(dropoutSerialized);
                default:
                    throw new InvalidOperationException("Unknown layer type to deserialize");
            }
        }



        private static ILayer DeserializeStandardLayer(SerializedStandardLayer standardSerialized, int batchSize)
        {
            var weights = Matrix<double>.Build.DenseOfArray(standardSerialized.Weights);
            var bias = Matrix<double>.Build.DenseOfColumnArrays(new double[][] { standardSerialized.Bias });
            var activator = ActivatorFactory.Build(standardSerialized.ActivatorType);
            var gradientAdjustmentParameters = standardSerialized.GradientAdjustmentParameters;
            return new BasicStandardLayer(weights, bias, batchSize, activator, gradientAdjustmentParameters);
        }

        private static ILayer DeserializeL2PenaltyLayer(SerializedL2PenaltyLayer l2PenaltyLayer, int batchSize)
        {
            BasicStandardLayer underlyingLayer = (BasicStandardLayer)Deserialize(l2PenaltyLayer.UnderlyingSerializedLayer, batchSize);
            var kappa = l2PenaltyLayer.PenaltyCoefficient;
            return new L2PenaltyLayer(underlyingLayer, kappa);
        }

        private static ILayer DeserializeWeightDecayLayer(SerializedWeightDecayLayer weightDecayLayer, int batchSize)
        {
            BasicStandardLayer underlyingLayer = (BasicStandardLayer)Deserialize(weightDecayLayer.UnderlyingSerializedLayer, batchSize);
            var kappa = weightDecayLayer.DecayRate;
            return new WeightDecayLayer(underlyingLayer, kappa);
        }

        private static ILayer DeserializeInputStandardizingLayer(SerializedInputStandardizingLayer serializedInputStandardizingLayer, int batchSize)
        {
            BasicStandardLayer underlyingLayer = (BasicStandardLayer)Deserialize(serializedInputStandardizingLayer.UnderlyingSerializedLayer, batchSize);
            var mean = serializedInputStandardizingLayer.Mean;
            var stddev = serializedInputStandardizingLayer.StdDev;

            return new InputStandardizingLayer(mean, stddev, underlyingLayer);
        }

        private static ILayer DeserializeDropoutLayer(SerializedDropoutLayer serializedDropoutLayer)
        {
            return new Dropout(serializedDropoutLayer.LayerSize, serializedDropoutLayer.KeepProbability);
        }
    }
}