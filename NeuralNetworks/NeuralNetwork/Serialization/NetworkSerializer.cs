using NeuralNetwork.Common;
using NeuralNetwork.Common.Serialization;
using System;

namespace NeuralNetwork.Serialization
{
    public static class NetworkSerializer
    {
        
        public static SerializedNetwork Serialize(INetwork network)
        {
            LayerSerial LSerial = new LayerSerial();
            SerializedNetwork SNetwork = new SerializedNetwork();
            SNetwork.BatchSize = network.BatchSize;
            var SerializedLayers = new ISerializedLayer[network.Layers.Length];
            for (int i =0; i< network.Layers.Length; i++)
            {
                SerializedLayers[i] = LSerial.Serialize(network.Layers[i]);
            }
            SNetwork.SerializedLayers = SerializedLayers;
            return SNetwork;
        }
    }
}