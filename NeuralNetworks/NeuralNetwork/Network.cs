﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common;
using NeuralNetwork.Common.Layers;
using System;

namespace NeuralNetwork
{
    public sealed class Network : INetwork
    {
        private int _BatchSize { get; set; }
        public int BatchSize
        {
            get { return _BatchSize; }
            set
            {
                if (value != _BatchSize)
                {
                    CheckRemainingBatchSizeAndUpdate(value);
                }
                _BatchSize = value;
            }
        }
                 
        public int LayerNb { get; }
        public int Epoch { get; set; }
        internal ILayer OutputLayer => Layers[LayerNb - 1];
        public Matrix<double> Output => OutputLayer.Activation;
 

        public ILayer[] Layers { get; }

        public Mode Mode { get; set; }

        public Network(ILayer[] layers, int batchSize)
        {
            Layers = layers ?? throw new ArgumentNullException(nameof(layers));
            LayerNb = Layers.Length;
            if (LayerNb == 0)
            {
                throw new InvalidOperationException("The network must contain at least one layer");
            }
            _BatchSize = batchSize;
            BatchSize = batchSize;
            Epoch = 0;
        }

        public void CheckRemainingBatchSizeAndUpdate(int batchSize)
        {
            for (int i = 0; i < LayerNb; i++)
            {
                Layers[i].BatchSize = batchSize;
                Layers[i].SetActivationBias();
            }
        }

        public void Propagate(Matrix<double> input)
        {
            Layers[0].Propagate(input);
            for (int i = 1; i < LayerNb; i++)
            {
                Layers[i].Propagate(Layers[i - 1].Activation);
            }
        }

        public void Learn(Matrix<double> lossFunctionGradient)
        {
            foreach(ILayer layers in Layers)
            {
                layers.Epoch = Epoch;   
            }
            BackpropAndUpdate(OutputLayer, lossFunctionGradient);
            for (int i = LayerNb - 2; i >= 0; i--)
            {
                BackpropAndUpdate(Layers[i], Layers[i + 1].WeightedError);
            }
        }

        private void BackpropAndUpdate(ILayer layer, Matrix<double> outputLayerError)
        {
            layer.BackPropagate(outputLayerError);
            layer.UpdateParameters();
        }
    }
}