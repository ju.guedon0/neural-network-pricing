﻿using System;
using System.Collections.Generic;
using System.Text;
namespace NeuralNetwork.Activators
{
    class DropoutActivator : Random
    {
        public double Proba { get; set; }
        public DropoutActivator(double proba) {
            Proba = proba;

        }

        public Func<double,double> Apply => x => dropActivator(x);

        public double dropActivator(double x)
        {
            if( Proba< base.Sample())
            {
                return x / Proba;
            }
            else
            {
                return 0;
            }
        }

    }
}
