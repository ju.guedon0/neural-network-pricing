﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.Activators;
using NeuralNetwork.Common.GradientAdjustmentParameters;

namespace NeuralNetwork.Layers
{
    class L1Layer : BasicStandardLayer
    {
        public double Kappa;

        public L1Layer(double kappa, Matrix<double> weights, Matrix<double> bias, int batchSize, IActivator activator,
            IGradientAdjustmentParameters gradientAdjustmentParameters) : base(weights, bias, batchSize, activator,
            gradientAdjustmentParameters)
        {
            Kappa = kappa;
        }

        public new void UpdateParameters()
        {
            Matrix<double> Ones = Matrix<double>.Build.Dense(BatchSize, 1, 1);


            Matrix<double> grad_w = (Activation_previous * B.Transpose()).Multiply(1.0 / BatchSize) + Kappa * Weights.PointwiseSign();
            Matrix<double> grad_b_vect = (B * Ones).Multiply(1.0 / BatchSize);

            Weights = GradientW.ComputeGrad(grad_w, Weights);
            FillMatrixFromVector(Bias, GradientB.ComputeGrad(grad_b_vect, BiasVect));

        }
    }
}
