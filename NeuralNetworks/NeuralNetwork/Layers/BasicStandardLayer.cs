﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.Activators;
using NeuralNetwork.Common.Layers;
using System;
using System.Diagnostics;
using NeuralNetwork.Gradients;
using NeuralNetwork.Common.GradientAdjustmentParameters;

namespace NeuralNetwork.Layers
{
    internal class BasicStandardLayer : ILayer
    {
        public int LayerSize { get; }

        public int InputSize { get; }

        public int BatchSize { get; set; }

        public Matrix<double> Activation { get; set; }

        public Matrix<double> Activation_previous { get; set; }

        public Matrix<double> NetInput { get; set; }

        public Matrix<double> WeightedError { get; set; }

        public Matrix<double> Weights { get; set; }

        public Matrix<double> Bias { get; set; }

        protected Matrix<double> BiasVect
        {
            get { return Bias.Column(0).ToColumnMatrix(); }  
        }

        public Matrix<double> B { get; set; }

        public IActivator Activator { get; }

        public IGradient GradientW { get; set; }

        public IGradient GradientB { get; set; }

        public Matrix<double> WeightGradient { get; set; }

        public Matrix<double> BiasGradient { get; set; }

        public IGradientAdjustmentParameters GradientAdjustmentParameters { get; }

        public int Epoch { get; set; }

        public BasicStandardLayer(Matrix<double> weights, Matrix<double> bias, int batchSize, IActivator activator,
            IGradientAdjustmentParameters gradientAdjustmentParameters)
        {
            BatchSize = batchSize;
            InputSize = weights.RowCount;
            LayerSize = weights.ColumnCount;
            Activation = Matrix<double>.Build.Dense(LayerSize, BatchSize);
            Weights = weights;
            Activator = activator;
            Epoch = 0;
            Bias = Matrix<double>.Build.Dense(LayerSize, BatchSize);
            FillMatrixFromVector(Bias, bias);
            NetInput = Matrix<double>.Build.Dense(Weights.ColumnCount, BatchSize);
            GradientAdjustmentParameters = gradientAdjustmentParameters;
            GradParamToGradient conv = new GradParamToGradient();
            GradientW = conv.convertParamToGradient(GradientAdjustmentParameters, Weights.ColumnCount, Weights.RowCount);
            GradientB = conv.convertParamToGradient(gradientAdjustmentParameters, 1, Bias.RowCount);
        }
       
        protected void FillMatrixFromVector(Matrix<double> bias, Matrix<double> vector)
        {
            for (int i = 0; i < vector.RowCount; i++)
            {
                for (int j = 0; j < BatchSize; j++)
                {
                    bias[i, j] = vector[i, 0];
                }
            }
        }

        public void SetActivationBias()
        {
            Activation = Matrix<double>.Build.Dense(LayerSize, BatchSize);
            if (BatchSize < Bias.ColumnCount)
            {
                Bias = Bias.SubMatrix(0, Bias.RowCount, 0, BatchSize);
            }
            else
            {
                Matrix<double> res = Matrix<double>.Build.Dense(LayerSize, BatchSize);
                for (int i = 0; i < BatchSize; i++)
                {
                    res.SetColumn(i, Bias.Column(0));
                }
                Bias = res;
            }
            
            NetInput = Matrix<double>.Build.Dense(Weights.ColumnCount, BatchSize);
        }

        public void BackPropagate(Matrix<double> upstreamWeightedErrors)
        {
            Matrix<double> thetap = Matrix<double>.Build.Dense(Weights.ColumnCount, BatchSize);
            NetInput.Map(Activator.ApplyDerivative, thetap);
            B = thetap.PointwiseMultiply(upstreamWeightedErrors);
            WeightedError = Weights * B;

            Matrix<double> Ones = Matrix<double>.Build.Dense(BatchSize, 1, 1);

            WeightGradient = (Activation_previous * B.Transpose()).Multiply(1.0 / BatchSize);
            BiasGradient = (B * Ones).Multiply(1.0 / BatchSize);
        }


        public void Propagate(Matrix<double> input)
        {
            Activation_previous = input;
            NetInput = Weights.Transpose() * input + Bias;
            Activation = NetInput.Map(Activator.Apply);
        }
        
        public virtual void UpdateParameters()
        {
            switch(GradientW){
                case AdamGradient grad:
                    grad.NbTrainingSteps = Epoch;
                    break;
                default:
                    break;
            }
            switch (GradientB)
            {
                case AdamGradient grad:
                    grad.NbTrainingSteps = Epoch;
                    break;
                default:
                    break;
            }
            Weights = GradientW.ComputeGrad(WeightGradient, Weights);
            Console.WriteLine("weights");
            Console.Write(Weights);
            FillMatrixFromVector(Bias, GradientB.ComputeGrad(BiasGradient, BiasVect));
        }
        
    }
}