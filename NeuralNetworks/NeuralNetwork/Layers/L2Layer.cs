﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.Activators;
using NeuralNetwork.Common.GradientAdjustmentParameters;
using System;

namespace NeuralNetwork.Layers
{
    internal class L2Layer : BasicStandardLayer
    {
        public double Kappa;

        public L2Layer(double kappa, Matrix<double> weights, Matrix<double> bias, int batchSize, IActivator activator,
            IGradientAdjustmentParameters gradientAdjustmentParameters) : base(weights, bias, batchSize, activator,
            gradientAdjustmentParameters)
        {
            Kappa = kappa;
        }
        

        public override void UpdateParameters()
        {
            Matrix<double> Ones = Matrix<double>.Build.Dense(BatchSize, 1, 1);


            Matrix<double> grad_w = (Activation_previous * B.Transpose()).Multiply(1.0 / BatchSize) + Kappa*Weights;
            Matrix<double> grad_b_vect = (B * Ones).Multiply(1.0 / BatchSize) + Kappa *Bias;

            Weights = GradientW.ComputeGrad(grad_w, Weights);
            FillMatrixFromVector(Bias, GradientB.ComputeGrad(grad_b_vect, BiasVect));

        }
    }
}
