﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.Layers;
using System;

namespace NeuralNetwork.Layers
{
    internal class L2PenaltyLayer : ILayer
    {
        public L2PenaltyLayer(BasicStandardLayer underlyingLayer, double penaltyCoefficient)
        {
            UnderlyingLayer = underlyingLayer ?? throw new ArgumentNullException(nameof(underlyingLayer));
            PenaltyCoefficient = penaltyCoefficient;
            PenaltyWeights = Matrix<double>.Build.Dense(UnderlyingLayer.Weights.RowCount, UnderlyingLayer.Weights.ColumnCount);
            Epoch = 0;
        }

        public Matrix<double> PenaltyWeights { get; }
        public BasicStandardLayer UnderlyingLayer { get; set; }
        public double PenaltyCoefficient { get; }
        //public IMatrixStorage MatrixStorage => UnderlyingLayer.MatrixStorage;

        public int LayerSize => UnderlyingLayer.LayerSize;

        public int InputSize => UnderlyingLayer.InputSize;

        public int BatchSize
        {
            get => UnderlyingLayer.BatchSize;
            set => UnderlyingLayer.BatchSize = value;
        }

        public Matrix<double> Activation => UnderlyingLayer.Activation;

        public Matrix<double> WeightedError => UnderlyingLayer.WeightedError;

        public int Epoch { get; set; }
        public void BackPropagate(Matrix<double> upstreamWeightedErrors)
        {
            UnderlyingLayer.BackPropagate(upstreamWeightedErrors);
            UnderlyingLayer.Weights.Multiply(PenaltyCoefficient, PenaltyWeights);
            UnderlyingLayer.WeightGradient.Add(PenaltyWeights, UnderlyingLayer.WeightGradient);
          //  UnderlyingLayer.WeightGradient += PenaltyCoefficient*UnderlyingLayer.Weights.PointwiseSign();
        }

        public void Propagate(Matrix<double> input)
        {
            UnderlyingLayer.Propagate(input);
        }

        public void UpdateParameters()
        {
            UnderlyingLayer.Epoch = Epoch;
            UnderlyingLayer.UpdateParameters();
        }

        public void SetActivationBias()
        {
            UnderlyingLayer.SetActivationBias();
        }

    }
}