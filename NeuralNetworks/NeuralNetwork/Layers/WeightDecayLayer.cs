﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Common.Layers;
using System;

namespace NeuralNetwork.Layers
{
    internal class WeightDecayLayer : ILayer
    { 
        public WeightDecayLayer(BasicStandardLayer underlyingLayer, double decayRate)
        {
            UnderlyingLayer = underlyingLayer ?? throw new ArgumentNullException(nameof(underlyingLayer));
            DecayRate = decayRate;
            DecayedWeights = Matrix<double>.Build.Dense(UnderlyingLayer.Weights.RowCount, UnderlyingLayer.Weights.ColumnCount);
            Epoch = 0;
        }

        public Matrix<double> DecayedWeights { get; }
        public BasicStandardLayer UnderlyingLayer { get; }
        public double DecayRate { get; }

        public int LayerSize => UnderlyingLayer.LayerSize;

        public int InputSize => UnderlyingLayer.InputSize;

        public int BatchSize { get => UnderlyingLayer.BatchSize; set => UnderlyingLayer.BatchSize = value; }

        public Matrix<double> Activation => UnderlyingLayer.Activation;

        public Matrix<double> WeightedError => UnderlyingLayer.WeightedError;

        public int Epoch { get; set; }
        public void BackPropagate(Matrix<double> upstreamWeightedErrors)
        {
            UnderlyingLayer.BackPropagate(upstreamWeightedErrors);
        }

        public bool Equals(ILayer other)
        {
            return other is WeightDecayLayer weight && Equals(weight);
        }

        public bool Equals(WeightDecayLayer other)
        {
            return UnderlyingLayer.Equals(other.UnderlyingLayer) && DecayRate == other.DecayRate;
        }

        public void Propagate(Matrix<double> input)
        {
            UnderlyingLayer.Propagate(input);
        }

        public void UpdateParameters()
        {
            UnderlyingLayer.Epoch = Epoch;
            UnderlyingLayer.Weights.Multiply(DecayRate, DecayedWeights);
            UnderlyingLayer.UpdateParameters();
            UnderlyingLayer.Weights.Subtract(DecayedWeights, UnderlyingLayer.Weights);
        }
        public void SetActivationBias()
        {
            UnderlyingLayer.SetActivationBias();
        }
    }
}