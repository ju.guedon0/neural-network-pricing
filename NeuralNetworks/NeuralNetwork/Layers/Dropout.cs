﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetwork.Activators;
using NeuralNetwork.Common.Layers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.Layers
{
    class Dropout : ILayer
    {
        public int LayerSize { get; }

        public int InputSize { get; }

        public int BatchSize { get; set; }
        public int Epoch { get; set; }
        public double Proba { get; set; }

        public Matrix<double> Activation { get; set; }

        public Matrix<double> WeightedError { get; set; }

        public Vector<double> Mask {get;set;}
        private DropoutActivator Activator {get;set;}

        public void generateMask()
        {
            Mask = Vector<double>.Build.Dense(LayerSize, 0);
            for (int i = 0; i< Mask.Count; i++)
            {
                Mask[i] = Activator.Apply(1);
            }
        }
        public Dropout(int layerSize, double probability)
        {
            Proba = probability;
            LayerSize = layerSize;
            Activator = new DropoutActivator(probability);
            generateMask();
        }

        public void BackPropagate(Matrix<double> upstreamWeightedErrors)
        {
            for (int i = 0; i< upstreamWeightedErrors.RowCount; i++)
            {
                for(int j = 0; j < upstreamWeightedErrors.ColumnCount; j++)
                {
                    upstreamWeightedErrors[i, j] *= Mask[i];
                }
            }
            WeightedError = upstreamWeightedErrors;
        }

        public void Propagate(Matrix<double> input)
        {
            for (int i = 0; i < input.RowCount; i++)
            {
                for (int j = 0; j < input.ColumnCount; j++)
                {
                    input[i, j] *= Mask[i];
                }
            }
            Activation = input;
        }

        public void SetActivationBias()
        {

        }

        public void UpdateParameters()
        {

        }
    }
}
