import json
import numpy as np

def creategradientParam(Learningrate,Momentum):
    dict={}
    dict["LearningRate"] =Learningrate
    dict["Momentum"] = Momentum
    dict["Type"] = "Momentum"
    return dict
def createNesterovgradientParam(Learningrate,Momentum):
    dict={}
    dict["LearningRate"] =Learningrate
    dict["Momentum"] = Momentum
    dict["Type"] = "Nesterov"
    return dict
def createAdamgradientParam(stepsize,D1,D2,deno):
    dict={}
    dict["StepSize"] = stepsize
    dict["FirstMomentDecay"] = D1
    dict["SecondMomentDecay"] = D2
    dict["DenominatorFactor"] = deno
    dict["Type"] = "Adam"
    return dict

def createLayerWithUnderlying(UnderlyingLayer,PenaltyCoefficient,Type):
    dict={}
    dict["Type"] = Type
    dict["UnderlyingSerializedLayer"] = UnderlyingLayer
    if(Type =="WeightDecay"):
        dict["DecayRate"] = PenaltyCoefficient
    else:
        dict["PenaltyCoefficient"] = PenaltyCoefficient
    return dict
def createLayer(Bias,Weights,gradientParam,ActivatorType="LeakyReLU"):
    dict ={}
    dict["Bias"] = Bias
    dict["Weights"] = Weights
    dict["ActivatorType"] = ActivatorType
    dict["GradientAdjustmentParameters"] = gradientParam
    dict["Type"] = "Standard"
    return dict

def createDropoutLayer(LayerSize,Proba):
    dict={}
    dict["Type"] = "Dropout"
    dict["KeepProbability"] = Proba
    dict["LayerSize"] = LayerSize
    return dict
def createStandardInputLayer(UnderlyingLayer):
    dict={}
    dict["Type"] = "InputStandardizing"
    dict["UnderlyingSerializedLayer"] = UnderlyingLayer
    dict["Mean"] = [91.83023508,6.453272462,134.6198411,0.2715644209,1.096581775,0.03005387962,0.3580176181]
    dict["StdDev"] = [40.52715639,2.581709409,65.99758929,0.07908834752,0.5623273105,0.005798077484,0.08519782706]
    return dict


def W(lines, cols):
    #return list(np.random.randn(lines,cols) *np.sqrt(2/lines))
    l= list(np.random.randn(lines,cols).tolist()) 
    tab = [[np.sqrt(2/lines) *l[i][j] for j in range(len(l[i]))] for i in range(len(l))]
    return tab
    #return list([list(np.random.normal(0, 1, cols)) for _ in range(lines)])
def B(lines):
    return list([0 for _ in range(lines)])

def create_json(Batchsize,Layers):
    dict={}
    dict["BatchSize"] = Batchsize
    dict["SerializedLayers"] = Layers
    json_obj = json.dumps(dict,indent=4)
    return json_obj

grad = createAdamgradientParam(0.001,0.9,0.999,1e-08)

#grad = creategradientParam(0.01,0.5)


n= 7
L1= createStandardInputLayer(createLayer(B(n),W(7,n),grad))
L2= createLayer(B(n),W(n,n),grad)
L3= createLayer(B(n),W(n,n),grad)
L = createDropoutLayer(n,0.5)
L6=createLayer(B(1),W(n,1),grad)
Layers=[L1,L2,L3,L6]

for i in range(1,len(Layers)):
  Layers[i] = createLayerWithUnderlying(Layers[i],1e-08,"WeightDecay")
  #Layers[i] = createLayerWithUnderlying(Layers[i],0.01,"L2Penalty")
Layers.insert(2,L)
network = create_json(20,Layers)

with open("samplel2.json", "w") as outfile:
    outfile.write(network)
