This solution provides a user interface to train neural networks and visualize their training and validation errors. For the solution to compile, the following steps must be taken:
 - NeuralNetwork.Common.dll must be added as a reference to the Trainer project
 - DataProviders.dll, NeuralNetwork.Common.dll and NeuralNetwork.dll must be added as references to the Visualizer project